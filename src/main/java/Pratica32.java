/**
 *
 * @author Lucio Rocha
 * @since 26/09/2016
 */
public class Pratica32 {
    
    public static double densidade(double x, double media, 
            double desvio) {
        double d = 1/(Math.sqrt(2*Math.PI)*desvio)*Math.exp(-1/2*(Math.pow((x-media)/desvio,2)));
        return d;
    }
    
    public static void main(String [] args){
        double x, media, desvio;
        x=-1; 
        media=67;
        desvio=3;
        System.out.println(densidade(x,media,desvio));
        
    }
    
}
